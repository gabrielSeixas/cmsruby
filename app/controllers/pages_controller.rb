class PagesController < ApplicationController
  before_action :set, only: [:show]	

  def index
  	@pages = Page.paginate(:page => params[:page], :per_page => 30)
  end

  def show
  end

  def create
  end

  def show_json
  	@page = Page.find(params[:id])
    # @page = Page.paginate(:page => params[:page], :per_page => 30)

  	respond_to do |format|
  	  format.json { render json: @page.to_json }
  	end
  end

  private

    def set
      @page = Page.find(params[:id])
    end

end