Rails.application.routes.draw do
	
  # get "/" :categories, only: [:show]
  resources :categories, only: [:show, :index]

  resources :pages, only: [:show]
  
  get "/page/:id/api" => "pages#show_json", defaults: { format: "json" }

  root "pages#index"

  namespace :admin do
  	resources :categories
    resources :pages  # admin/pages
  end

  Page.where.not("slug", nil).all.each do |page|
  	get "/#{page.slug}", controller: "pages", action: "show", id: page.id
  end

end
